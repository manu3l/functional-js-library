let Maybe = (() => {

  const exists = value => value !== null && value !== undefined;
  const ILLEGAL_CONSTRUCTOR_USAGE = "Illegal usage of constructor! "
    + "Use Maybe.of(value) or Maybe.fromNullable(value)!";
  const MAYBE_OF_NULL = "Maybe.of() expects a defined, non null value!";
  const GET_VALUE_OF_NOTHING = "Can not extract the value of a nothing!";
  
  class Maybe {
    constructor(_) { throw new Error(ILLEGAL_CONSTRUCTOR_USAGE); }
    static of(value) {
      if(exists(value)) { return new Just(value); }
      else { throw new TypeError(MAYBE_OF_NULL); } 
    }
    static fromNullable(value) { return (exists(value)) ? new Just(value) : new Nothing(); }
  }

  class Just {
    constructor(value) { this._value = value; }
    get value() { return this._value; }
    get isPresent() { return true; }
    map(f) { return Maybe.of(f(this._value)); }
    getOrElse(_) { return this._value; }
    filter(f) { return Maybe.fromNullable(f(this._value) ? this._value : null); }
    toString() { return "Maybe.Just(" + this.value + ")"; }
  }

  class Nothing {
    constructor() {}
    get value() { throw new TypeError(GET_VALUE_OF_NOTHING); }
    get isPresent() { return false; }
    map(_) { return this; }
    getOrElse(other) { return other; }
    filter(_) { return this; }
    toString() { return "Maybe.Nothing"; }
  }
  
  return Maybe;
})();

let Try = (() => {

  const exists = value => value !== null && value !== undefined;
  const ILLEGAL_CONSTRUCTOR_USAGE = "Illegal usage of constructor! "
    + "Use Try.success(value), Try.failure(message) or Try.fromNullable(value)!";
  const TRY_OF_NULL = "Try.of() expects a defined, non null value!";
  const GET_VALUE_OF_FAILURE = "Can not extract the value of a Failure!";

  class Try {
    constructor(_) { throw new TypeError(ILLEGAL_CONSTRUCTOR_USAGE); }
    static success(value) { return new Success(value); }
    static failure(value) { return new Failure(value); }
    static fromNullable(value) { return (exists(value)) ? new Success(value) : new Failure(value); }
    static of (value) {
      if(exists(value)) { return new Success(value); }
      else { throw new TypeError(TRY_OF_NULL); }
    }
  }
  
  class Success {
    constructor(value) { this._value = value; }
    get value() { return this._value; }
    get isSuccess() { return true; }
    get isFailure() { return false; }
    map(f) { return Try.of(f(this._value)); }
    getOrElse(_) { return this._value; }
    orElse(_) { return this; }
    chain(f) { return f(this._value); }
    filter(f) { return Try.fromNullable(f(this._value) ? this._value : null); }
    toString() { return "Try.Success(" + this._value + ")"; }
  }
  
  class Failure {
    constructor(value) { this._value = exists(value) ? value : undefined; }
    get value() { throw new TypeError(GET_VALUE_OF_FAILURE); }
    get isSuccess() { return false; }
    get isFailure() { return true; }
    map(_) { return this; }
    getOrElse(other) { return other; }
    orElse(f) { return f(this._value); }
    chain(f) { return this; }
    filter(f) { return this; }
    toString() { return "Try.Failure(" + this._value + ")"; }
  }
  
  return Try;
})();
