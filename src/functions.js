let functions = (() => {
  let identity = x => x;
  let pipeTwo = (f, g) => x => g(f(x));
  let pipe = (...fns) => fns.reduce(pipeTwo);
  function partial(f) {
    return (function resolve(...first) {
        return function(...next) {
            var all = first.concat(next);
            return (all.length < f.length) ? resolve(...all) : f(...all);
        }
    })();
  };

  let trace = partial((m, arg) => {console.log(m, arg); return arg; });

  let ifThenElse = partial((predicate, f, g) => value => predicate(value) ? f(value) : g(value));
  let is = a => b => a === b;
  let not = a => !a;

  let toArray = arr => Array.from(arr);
  let forEach = partial((f, array) => array.forEach(f));
  let filter = partial((f, array) => array.filter(f));
  let map = partial((f, array) => array.map(f));
  let reduce = partial((f, array) => array.reduce(f));
  let concat = (...arrays) => arrays.reduce((arr1, arr2) => arr1.concat(arr2));

  let replace = partial((before, after, text)  => text.split(before).join(after));
  let split = partial((delimiter, text) => text.split(delimiter));
  let splitEach = partial((delimiter, array)  => array.map(split(delimiter)));
  let match = partial((pattern, string) => string.match(pattern) || []);
  let occurences = partial((pattern, string) => string.match(pattern).length);

  let property = partial((obj, prop) => obj && prop ? obj[prop] : undefined);
  let path = (...props) => obj => props.reduce(property, obj);
  let isObject = obj => obj && typeof obj === "object";
  let deepFreeze = obj => {
    if (isObject(obj)) {
      Object.getOwnPropertyNames(obj).map(name => obj[name]).forEach(deepFreeze);
      Object.freeze(obj);
    }
    return obj;
  };
  let immutableCopy = pipe(JSON.stringify, JSON.parse, deepFreeze);
  return { 
    identity, pipeTwo, pipe, partial, 
    ifThenElse, is, not, 
    toArray, forEach, filter, map, reduce, concat,
    replace, split, splitEach, match, occurences,
    trace, property, path, deepFreeze, immutableCopy
  };
})();
