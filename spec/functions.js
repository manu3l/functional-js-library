describe("functions", () =>  {
  describe("identity", () =>  {
    var helloWorld = { hello: "world"};
    let add = (a, b) => a + b;
    it("should return first argument", () =>  {
        expect(functions.identity(2)).toBe(2);
        expect(functions.identity(helloWorld)).toBe(helloWorld);
        expect(functions.identity(add)).toBe(add);
        expect(functions.identity(1,2,3)).toBe(1);
    });
  });
  describe("pipeTwo", () =>  {
    let pipeTwo = functions.pipeTwo(a => a + 2, a => a * a);
    it("should apply two functions one after another", () =>  {
      expect(pipeTwo(3)).toBe(25);
    });
  });
  describe("pipe", () =>  {
    let pipeThree = functions.pipe(a => a + 2, a => a * a, a => a + "!");
    it("should apply three functions one after another", () =>  {
      expect(pipeThree(3)).toBe("25!");
    });
  });    
  describe("partial", () =>  {
    var helloWorld = functions.partial((h, w, mark) => h + ", " + w + mark, 3);
    it("should call original function, if all parameters are specified", () =>  {
      expect(helloWorld("Hello", "world", "!")).toBe("Hello, world!");
    });
    it("should take second and third argument, if first argument is specified", () =>  {
      expect(helloWorld("Hello")("world", "!")).toBe("Hello, world!");
    });
    it("should take third argument, if first and second argument are specified", () =>  {
      expect(helloWorld("Hello")("world", "!")).toBe("Hello, world!");
    });
    it("should take all arguments, one after another", () =>  {
      expect(helloWorld("Hello")("world")("!")).toBe("Hello, world!");
    });
  });

  describe("ifThenElse", () => {
    it("should return first alternative if predicate returns true for input", () => {
      expect(functions.ifThenElse(_ => true, () => "true", () => false)()).toBe("true");
    });
    it("should return second alternative if predicate returns false for input", () => {
      expect(functions.ifThenElse(_ => false, () => "true", () => "false")()).toBe("false");
    });
  });
  describe("is", () => {
    it("should say true if primitives are identical", () => {
      expect(functions.is(2)(2)).toBe(true);
      expect(functions.is(false)(false)).toBe(true);
      expect(functions.is("Hello")("Hello")).toBe(true);
    });
    it("should say false if two primitives are different", () => {
      expect(functions.is(1)(2)).toBe(false);
      expect(functions.is("2")(2)).toBe(false);
      expect(functions.is("")(undefined)).toBe(false);
      expect(functions.is(1)(2)).toBe(false);
    });
  });
  describe("not", () => {
    it("should values", () => {
      expect(functions.not(true)).toBe(false);
      expect(functions.not(false)).toBe(true);
      expect(functions.not(undefined)).toBe(true);
      expect(functions.not(1)).toBe(false);
    });
  });
  
  describe("toArray", () =>  {
    var arr = ["a", "b", "c"];
    it("should return equal array", () =>  {
      expect(functions.toArray(arr)).toEqual(["a", "b", "c"]);
    });
    it("should not return the same array", () =>  {
      expect(functions.toArray(arr)).not.toBe(arr);
    });
    it("should return array containing values for arguments object", () =>  {
      function testCopy(a,b,c) { return functions.toArray(arguments); }
      expect(testCopy("a", "b", "c")).toEqual(["a", "b", "c"]);
    });
  });
  describe("forEach", () =>  {
    var result;
    let consumer = a => result.push(a);
    let input = [1, 2, 3];
    beforeEach(() =>  { result = []; });
    it("should iterate over an array", () =>  {
      functions.forEach(consumer)(input);
      expect(result).toEqual([1, 2, 3]);
    });
  });
  describe("filter", () =>  {
    it("should filter out all odd values", () =>  {
      expect(functions.filter(x => x%2 === 0)([1,2,3,4,5,6])).toEqual([2,4,6]);
    });
  });
  describe("map", () =>  {
    it("should map all names to upper case", () =>  {
      expect(functions.map(x => x.toUpperCase())(["Alex", "Sandra", "Julia"])).toEqual(["ALEX", "SANDRA", "JULIA"]);
    });
  });
  describe("reduce", () =>  {
    it("should collect all names in one string", () =>  {
      expect(functions.reduce((a, b) => a + ", " + b)(["Alex", "Sandra", "Julia"])).toEqual("Alex, Sandra, Julia");
    });
  });
  describe("concat", () =>  {
    var arr1 = ["a", "b", "c"], arr2 = ["d", "e", "f"], arr3 = ["g", "h", "i"];
    it("should return array with values from all input arrays", () =>  {
      expect(functions.concat(arr1, arr2, arr3)).toEqual(["a", "b", "c", "d", "e", "f", "g", "h", "i"]);
    });
    it("should return fresh array instance", () =>  {
      var result = functions.concat(arr1, arr2);
      expect(result).not.toBe(arr1);
      expect(result).not.toBe(arr2);
    });
  });
  
  describe("replace", () =>  {
    let greeting = "Hello, <name>!";
    it("should replace a text value", () =>  {
      expect(functions.replace("<name>", "World", greeting)).toBe("Hello, World!");
      expect(functions.replace("<name>")("World", greeting)).toBe("Hello, World!");
      expect(functions.replace("<name>", "World")(greeting)).toBe("Hello, World!");
    });
    it("should replace all occurences", () =>  {
      expect(functions.replace("a","e", "bla, bla, blaaah")).toBe("ble, ble, bleeeh");
    });
  });
  describe("split", () =>  {
    it("should split lines, if newline is the delimiter", () =>  {
      expect(functions.split("\n")("line 1\nline 2\nline 3")).toEqual(["line 1", "line 2", "line 3"]);
    });
  });
  describe("splitEach", () =>  {
    it("should split each text arrays entries", () =>  {
      let entries = ["a", "b,c", "d,e,f", "g,"];
      expect(functions.splitEach(",")(entries)).toEqual([["a"], ["b", "c"], ["d", "e", "f"], ["g", ""]]);
    });
  });
  describe("match", () => {
    it("should return an array of all occurences of a regex", () => {
      expect(functions.match(/abc/gi, "abcdefABChi")).toEqual(["abc", "ABC"]);
    });
    it("should return empty array, if no matches are found", () => {
      expect(functions.match(/abc/gi, "def")).toEqual([]);
    });
  });
  describe("occurences", () => {
    it("should count all occurences of a regex", () => {
      expect(functions.occurences(/abc/gi, "abcdefABChi")).toBe(2);
    });
  });
  
  describe("trace", () =>  {
    var testLog;
    var logTrace = function(...args) { testLog = args; };
    var originalLog = console.log;

    beforeEach(() =>  { console.log = logTrace; });
    afterEach(() =>   { console.log = originalLog; });

    var loggedPipe = functions.pipe(() => "argument", functions.trace("Message"));
    it("should return second argument", () =>  {
      expect(loggedPipe()).toBe("argument");
    });
    it("should log second argument", () =>  {
      loggedPipe();
      expect(testLog).toEqual(["Message", "argument"]);
    });
  });
  describe("property", () => {
    it("should return existing property of object", () => {
      expect(functions.property({ a: 12 }, "a")).toBe(12);
    });
    it("should return undefined if property or object is null", () => {
      expect(functions.property(null, "a")).toEqual(undefined);
      expect(functions.property({ a: 12 }, null)).toEqual(undefined);
    });
   it("should return undefined if property does not exist on object", () => {
      expect(functions.property({ a: 12 }, "b")).toBe(undefined);
    });
  });
  describe("path", () => {
    it("should return property of object", () => {
      expect(functions.path("a")({ a: "propA" })).toEqual("propA");
    });
    it("should return nested property of object", () => {
      expect(functions.path("a", "b", "c")({ a: { b: { c: "propD" } } })).toEqual("propD");
      expect(functions.path("a", 2, "c")({ a: { 2: { c: "propD" } } })).toEqual("propD");
    });
    it("should return undefined if one property in path does not exist", () => {
      expect(functions.path("a", "b", "d")({ a: { b: { c: "propD" } } })).toEqual(undefined);
      expect(functions.path("d", "b", "c")({ a: { b: { c: "propD" } } })).toEqual(undefined);
    });
    it("should return undefined if expected object is no object", () => {
      expect(functions.path("a", "b", "d")([ 1, 2 ])).toEqual(undefined);
      expect(functions.path("d", "b", "c")(null)).toEqual(undefined);
      expect(functions.path("d", "b", "c")(x => x)).toEqual(undefined);
    });
    it("should return undefined if path has not only strings and numbers", () => {
      expect(functions.path("a", null, "d")({ a: { b: { c: "propD" } } })).toEqual(undefined);
      expect(functions.path("d", "b", [2])({ a: { b: { c: "propD" } } })).toEqual(undefined);
    });
  });
  describe("immutableCopy", () => {
    let testObject, copy;
    beforeEach(() => {
      testObject = { a: "a", b: [1, null, "2"], c: { d: "e"} };
      copy = functions.immutableCopy(testObject);
    });
    it("should return equal object", () => {
      expect(copy).toEqual(testObject);
    });
    it("should return not the same object", () => {
      expect(copy).not.toBe(testObject);
    });
   it("should return immutable object", () => {
      copy.a = "b";
      copy.c.d = "f";
      expect(copy).toEqual(testObject);
      expect(() => { copy.b.push(4); }).toThrowError(TypeError);
    });
  });
});
