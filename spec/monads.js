describe("Maybe monad", () => {
  describe("constructor", () => {
    it("should throw Error when using it directly", () => {
      expect(Maybe).toThrowError();
    });
  });
  
  describe(".of", () => {
    describe("null or undefined", () => {
      it("should throw TypeError", () => {
        expect(() => { Maybe.of(null); }).toThrowError(TypeError);
        expect(() => { Maybe.of(undefined); }).toThrowError(TypeError);
      });
    });

    testJust("2", Maybe.of(2), 2);
  });
  describe(".fromNullable", () => {
    testNothing("null", Maybe.fromNullable(null));
    testNothing("undefined", Maybe.fromNullable(undefined));
    testJust("2", Maybe.fromNullable(2), 2);
  });
});

function testJust(label, m, value) {
  describe(label, () => {
    it("should return value when trying to read it", () => {
      expect(m.value).toBe(value);
    });
    it("should be present", () => {
      expect(m.isPresent).toBe(true);
    });
    it("should map to a value", () => {
      expect(m.map(a => a + 2).toString()).toBe("Maybe.Just(" + (value + 2) + ")");
    });
    it("should getOrElse the value", () => {
      expect(m.getOrElse(3)).toBe(value);
    });
    it("should be filtered to the same Just by true predicate", () => {
      expect(m.filter(() => true).toString()).toBe(m.toString());
    });
    it("should be filtered to a Nothing by false predicate", () => {
      expect(m.filter(() => false).toString()).toBe("Maybe.Nothing");
    });
    it("should print \"Maybe.Just(" + value + ")\" in toString()", () => {
      expect(m.toString()).toBe("Maybe.Just(" + value + ")");
    });
  });
}

function testNothing(label, m) {
  describe(label, () => {    
    it("should throw an type error when trying to read value", () => {
      expect(() => { m.value; }).toThrowError(TypeError);
    });
    it("should not be present", () => {
      expect(m.isPresent).toBe(false);
    });
    it("should map to a nothing", () => {
      expect(m.map(a => a + 2).toString()).toBe("Maybe.Nothing");
    });
    it("should getOrElse the alternative", () => {
      expect(m.getOrElse(3)).toBe(3);
    });
    it("should always be filtered to a Nothing", () => {
      expect(m.filter(() => true).toString()).toBe("Maybe.Nothing");
      expect(m.filter(() => false).toString()).toBe("Maybe.Nothing");
    });
    it("should print \"Maybe.Nothing\" in toString()", () => {
      expect(m.toString()).toBe("Maybe.Nothing");
    });
  });
}

describe("Try monad", () => {
  describe("constructor", () => {
    it("should throw Error when using it directly", () => {
      expect(Try).toThrowError();
    });
  });
  
  describe(".of", () => {
    describe("null or undefined", () => {
      it("should throw TypeError", () => {
        expect(() => { Try.of(null); }).toThrowError(TypeError);
        expect(() => { Try.of(undefined); }).toThrowError(TypeError);
      });      
    });

    testSuccess("2", Try.of(2), 2);
  });
  describe(".fromNullable", () => {
    testFailure("null", Try.fromNullable(null));
    testFailure("undefined", Try.fromNullable(undefined));
    testSuccess("2", Try.fromNullable(2), 2);
  });
  testSuccess("success(2)", Try.success(2), 2);
  testFailure("failure(failure)", Try.failure("failure"), "failure");
});

function testSuccess(label, t, value) {
  describe(label, () => {
    it("should return value when trying to read it", () => {
      expect(t.value).toBe(value);
    });
    it("should be success", () => {
      expect(t.isSuccess).toBe(true);
    });
    it("should not be failure", () => {
      expect(t.isFailure).toBe(false);
    });
    it("should map to a value", () => {
      expect(t.map(a => a + 2)).toEqual(Try.success((value + 2)));
    });
    it("should getOrElse the value", () => {
      expect(t.getOrElse(3)).toBe(value);
    });
    it("should orElse this Success", () => {
      expect(t.orElse(error => "extended " + error)).toBe(t);
    });
    it("should chain a Success-retruning function", () => {
      expect(t.chain(a => Try.success(a + 2))).toEqual(Try.success(value + 2));
    });
    it("should chain a Failure-retruning function", () => {
      expect(t.chain(a => Try.failure("error " + a))).toEqual(Try.failure("error " + value));
    });
    it("should be filtered to equal Success by true predicate", () => {
      expect(t.filter(() => true)).toEqual(t);
    });
    it("should be filtered to a Failure by false predicate", () => {
      expect(t.filter(() => false)).toEqual(Try.fromNullable(null));
    });
    it("should print \"Try.Success(" + value + ")\" in toString()", () => {
      expect(t.toString()).toBe("Try.Success(" + value + ")");
    });
  });
}

function testFailure(label, t, value) {
  describe(label, () => {
    it("should throw an type error when trying to read value", () => {
      expect(() => { t.value; }).toThrowError(TypeError);
    });
    it("should not be success", () => {
      expect(t.isSuccess).toBe(false);
    });
    it("should be failure", () => {
      expect(t.isFailure).toBe(true);
    });
    it("should map to itself", () => {
      expect(t.map(a => a + 2)).toBe(t);
    });
    it("should getOrElse the alternative", () => {
      expect(t.getOrElse(3)).toBe(3);
    });
    it("should apply orElse function to a failure and return it", () => {
      expect(t.orElse(error => "Formatted " + error)).toEqual("Formatted " + value);
    });
    it("should always chain to itself", () => {
      expect(t.chain(a => Try.success(a + 2))).toBe(t);
      expect(t.chain(a => Try.failure("error " + a))).toBe(t);
    });
    it("should always be filtered to itself", () => {
      expect(t.filter(() => true)).toBe(t);
      expect(t.filter(() => false)).toBe(t);
    });
    it("should print \"Try.failure(" + value + ")\" in toString()", () => {
      expect(t.toString()).toBe("Try.Failure(" + value + ")");
    });
  });
}
